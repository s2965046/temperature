package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestTemperatureFormula {
    @Test
    public void testTemperature1() throws Exception {
        TemperatureFormula temperatureFormula = new TemperatureFormula();
        double temperature = temperatureFormula.getTemperatureFahrenheit(1.0);
        Assertions.assertEquals(33.8, temperature, 0.0, "Fahrenheit");
    }
}
