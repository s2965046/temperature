package nl.utwente.di.temperature;

public class TemperatureFormula {
    double getTemperatureFahrenheit(double celsius) {
        return celsius * 9 / 5 + 32;
    }
}
