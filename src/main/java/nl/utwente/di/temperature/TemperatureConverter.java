package nl.utwente.di.temperature;

import java.io.*;

import jakarta.servlet.*;
import jakarta.servlet.http.*;

/**
 * Servlet that gets a temperature in Celsius and returns it in Fahrenheit
 */

public class TemperatureConverter extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private TemperatureFormula temperatureFormula;

    public void init() throws ServletException {
        temperatureFormula = new TemperatureFormula();
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String title = "Temperature in Fahrenheit";

        // Done with string concatenation only for the demo
        // Not expected to be done like this in the project
        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Temperature in Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                temperatureFormula.getTemperatureFahrenheit(Double.parseDouble(request.getParameter("celsius"))) +
                "</BODY></HTML>");
    }
}
